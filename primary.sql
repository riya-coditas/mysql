/* PRIMARY KEY AND FOREIGN KEY  

PRIMARY KEY */
CREATE TABLE parent_table(
parent_id INT NOT NULL PRIMARY KEY);

/* Query OK, 0 rows affected (0.06 sec) */


/* FOREIGN KEY  */
CREATE TABLE child_table(
child_id INT NOT NULL PRIMARY KEY,
parent_id INT REFERENCES parent_table(parent_id));

/* Query OK, 0 rows affected (0.05 sec)  */