/* PROCEDURES */

create database pets;
use pets;
create table pets(
  id int primary key,
  name varchar(50),
  owner varchar(50),
  species varchar(50),
  sex char(1),
  birth date,
  death date);

/* Query OK, 0 rows affected (0.04 sec)  */

insert into pets(id,name,owner,species,sex,birth,death) values(1,"Snowflake","Ewin","Robo","F","2015-03-28",NULL),(2,"Cuddles","Frank","Kittly","M","2016-12-11","2016-12-12");
/* Query OK, 2 rows affected (0.01 sec)
Records: 2  Duplicates: 0  Warnings: 0  */

CREATE PROCEDURE pet_female()
BEGIN
  SELECT * FROM pets 
  where sex="F";
END //


CREATE PROCEDURES pet_female_with_birthdate()
BEGIN
   SET @birthdate := "2015-03-28";
   SELECT * FROM pets WHERE sex = "F" AND  birth = @birthdate;
END //
