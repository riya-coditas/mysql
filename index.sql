/*INDEX   */
DROP TABLE IF EXISTS Interests;
/* Query OK, 0 rows affected, 1 warning (0.01 sec) */

DROP TABLE IF EXISTS Animals;
/* Query OK, 0 rows affected, 1 warning (0.00 sec) */

DROP TABLE IF EXISTS Species;
/* Query OK, 0 rows affected, 1 warning (0.01 sec) */


CREATE TABLE Species(
   id INT PRIMARY KEY,
   Species varchar(50) NOT NULL UNIQUE,
   FriendlyName varchar(50) NOT NULL);
/* Query OK, 0 rows affected (0.04 sec) */


CREATE TABLE Animals(
   id INT PRIMARY KEY,
   name varchar(50) NOT NULL,
   SpeciesID INT NOT NULL REFERENCES Species(ID),
   ContactEmail varchar(50) NOT NULL UNIQUE );

/* Query OK, 0 rows affected (0.05 sec) */


CREATE TABLE Interests(
 AnimalID INT NOT NULL,
 SpeciesID INT NOT NULL,
 CONSTRAINT FK_InterestsAnimals FOREIGN KEY(AnimalID) REFERENCES Animals(ID),
 CONSTRAINT FK_InterestsSpecies FOREIGN KEY(SpeciesID) REFERENCES Species(ID)
 CONSTRAINT PK_Interests PRIMARY KEY(AnimalID , SpeciesID) );

 /* Query OK, 0 rows affected (0.04 sec) */

/*Insert values */
Insert into Species values(1,'Pentalagus','Bunny');
/* Query OK, 1 row affected (0.01 sec) */

Insert into Animals values('Sally',1,'sally@gmail.com');
/* Query OK, 1 row affected (0.01 sec) */

Select Animals.ID , Animals.ContactEmail from Animals;
+----+-----------------+
| ID | ContactEmail    |
+----+-----------------+
|  1 | sally@gmail.com |
+----+-----------------+


/*INDEX  */
CREATE INDEX IX_AnimalSpecies on Animals(SpeciesID);
/* Query OK, 0 rows affected (0.04 sec)
Records: 0  Duplicates: 0  Warnings: 0 */

DROP INDEX IX_AnimalSpecies on Animals;
/* Query OK, 0 rows affected (0.01 sec)
Records: 0  Duplicates: 0  Warnings: 0 */

CREATE INDEX IX_AnimalContact on Animals(name, ContactEmail);
/* Query OK, 0 rows affected (0.03 sec)
Records: 0  Duplicates: 0  Warnings: 0 */

CREATE UNIQUE INDEX IX_Species on Species(Species);
/* Query OK, 0 rows affected, 1 warning (0.05 sec)
Records: 0  Duplicates: 0  Warnings: 1  */