/*alias */

DROP TABLE users;

CREATE TABLE users1(user_id int not null primary key,
first_name varchar(50),
last_name varchar(50),
email varchar(50),
is_verified boolean,
registration TIMESTAMP);

/*
Output
Query OK, 0 rows affected (0.04 sec)  
*/

insert into users1 values(1,"Riya","Soni","riya@gmail.com","1",'2017-07-23 13:10:11');

/*Query OK, 1 row affected (0.01 sec) */

Select * from users1;

+---------+------------+-----------+----------------+-------------+---------------------+
| user_id | first_name | last_name | email          | is_verified | registration        |
+---------+------------+-----------+----------------+-------------+---------------------+
|       1 | Riya       | Soni      | riya@gmail.com |           1 | 2017-07-23 13:10:11 |
+---------+------------+-----------+----------------+-------------+---------------------+


Select email,first_name AS "FIRST NAME",
last_name AS "LAST NAME"
FROM users1;

+----------------+------------+-----------+
| email          | FIRST NAME | LAST NAME |
+----------------+------------+-----------+
| riya@gmail.com | Riya       | Soni      |
+----------------+------------+-----------+


/*Update query  */
Update users1 set email = 'dylan@gmail.com' where user_id = 4;

/* Output
Query OK, 0 rows affected (0.00 sec)
Rows matched: 0  Changed: 0  Warnings: 0 /*

