/* Creation of Primary key and Foreign key 

Primary key */

create table users(
    -> user_id int primary key,
    -> username varchar(50) not null unique);

/* Query OK, 0 rows affected (0.06 sec) */ 

/* Foreign key */
create table projects(
    -> project_id int primary key,
    -> project_name varchar(50),
    -> creator varchar(50),
    -> user_id int, foreign key(user_id) references users(user_id));

/* Query OK, 0 rows affected (0.05 sec) */