/* Insert values   */

insert into users1 values(1,"Riya");
/* Query OK, 1 row affected (0.01 sec) */


insert into projects values(1,"javascript","Riya",1);
/* Query OK, 1 row affected (0.01 sec) */


/* Delete values  */
delete from projects where project_id=1;
/* Query OK, 1 row affected (0.01 sec) */


/* Select values */

select * from users1;

+---------+------------+-----------+----------------+-------------+---------------------+
| user_id | first_name | last_name | email          | is_verified | registration        |
+---------+------------+-----------+----------------+-------------+---------------------+
|       1 | Riya       | Soni      | riya@gmail.com |           1 | 2017-07-23 13:10:11 |
+---------+------------+-----------+----------------+-------------+--------------


select * from projects;
+------------+--------------+---------+---------+
| project_id | project_name | creator | user_id |
+------------+--------------+---------+---------+
|          1 | javascript   | Riya    |       1 |
+------------+--------------+---------+---------+
